package br.up.edu.e2activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

public class DestinoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destino);

        Intent intent = getIntent();
        String texto = intent.getStringExtra("666");

        EditText txtMsg =  (EditText) findViewById(R.id.txtMensagem);
        txtMsg.setText(texto);

    }
}